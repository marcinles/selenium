from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

with webdriver.Chrome() as driver:
    wait = WebDriverWait(driver, 10)
    driver.get("http://www.anaesthetist.com/mnm/javascript/calc.htm")
    driver.find_element(By.NAME, "eight").click()
    driver.find_element(By.NAME, "mul").click()
    driver.find_element(By.NAME, "nine").click()
    driver.find_element(By.NAME, "result").click()
    wynik = driver.find_element(By.NAME, "Display").get_attribute("value")
    
    if (str(wynik) != "72"):
        raise ValueError
    driver.save_screenshot("screenshot.png")
  